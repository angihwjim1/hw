FROM python:3.10-slim

WORKDIR /app
COPY . /app

RUN pip install -q --upgrade pip \
    && pip install -q -r requirements.txt

CMD ["python", "run.py"]

EXPOSE 5000
