import random
import string
import uuid

from flask import Flask, jsonify

app = Flask(__name__)


@app.get('/api/latest')
def get_latest():
    response = {"data": generate_random_data()}
    return jsonify(response)


def generate_random_data(length=20):
    """Returns a list of dictionary with random data"""
    random_uuids = [uuid.uuid4() for _ in range(length)]
    random_ints = [random.randint(1, 100_000) for _ in range(length)]
    random_strings = [generate_random_str() for _ in range(length)]

    data = zip(random_uuids, random_ints, random_strings)

    return [
        {"id": str(generated_id), "value": value, "name": name}
        for generated_id, value, name in data
    ]


def generate_random_str(min_length=5, max_length=20):
    length = random.randint(min_length, max_length)
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


if __name__ == '__main__':
    app.run(host="0.0.0.0")
