import abc
from collections import defaultdict
import csv
from datetime import datetime
import math
import os
from pathlib import Path

import minio
import requests


class DataExporter(abc.ABC):
    """
    Abstract class for all exporters

    User should override `get` and `save` method. Optionally, user can also
    override the `transform` method.

    When run, this will run in order: `get`, `transform` and `save`.

    Optionally, override the `handle_error` method to handle errors (default
    is to just raise.)

    """

    @abc.abstractmethod
    def get(self):
        # override
        ...

    @abc.abstractmethod
    def save(self, data):
        # override
        ...

    def transform(self, data):
        # optional override
        return data

    def handle_error(self, error):
        # optional override
        raise error

    def run(self):
        try:
            response = self.get()
            transformed_data = self.transform(response)
            self.save(transformed_data)
        except Exception as err:
            self.handle_error(err)


class BaseApiExporter(DataExporter):

    def __init__(self, config):
        self.endpoint = config["endpoint"]
        self.key = config["key"]
        self.columns = config["columns"]
        self.save_location = config["save_to"]

    def get(self):
        response = requests.get(self.endpoint)
        if response.ok:
            return response.json()
        else:
            raise RuntimeError(f"Status code not ok, {response.status_code}")

    def transform(self, data):
        return data[self.key]


class LocalApiExporter(BaseApiExporter):
    """Data exporter that saves files locally."""

    def save(self, data):
        isodate = datetime.utcnow().isoformat()
        filename = f"dat_{isodate}.csv"
        self.write_local_file(data, filename)

    def write_local_file(self, data, filename, dirname=None):
        if not dirname:
            dirname = self.save_location
        filepath = Path(dirname) / filename
        with open(filepath, 'w') as f:
            writer = csv.DictWriter(f, self.columns)
            writer.writeheader()
            for row in data:
                writer.writerow(row)

        return filepath


class MinioApiExporter(LocalApiExporter):
    """Data exporter that saves to MinIO object storage."""

    def __init__(self, config):
        self.partition_key = config["partition_key"]
        self.client = minio.Minio(
            os.getenv("MINIO_SERVER", "minio-service:9000"),
            access_key=os.getenv("MINIO_ACCESS_KEY"),
            secret_key=os.getenv("MINIO_SECRET_KEY"),
            secure=False,    # TODO: set up SSL
        )
        self.bucket = config["bucket"]
        super().__init__(config)

    def transform(self, data):
        raw_data = data[self.key]
        output_data = defaultdict(list)
        for row in raw_data:
            key = math.floor(row[self.partition_key] / 10_000)
            output_data[key].append(row)
        return output_data

    def save(self, data):
        isodate = datetime.utcnow().isoformat()
        for partition, rows in data.items():
            filename = f"dat_{isodate}.csv"
            filepath = self.write_local_file(rows, filename, dirname="/tmp")

            key = f"{self.save_location}/{partition}/{filename}"
            self.client.fput_object(self.bucket, object_name=key, file_path=filepath)
