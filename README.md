# Homework Submission

The homework submission is creating a CronJob every minute to export data from a public API and then save the data to an s3 location using MinIO.

## First Time Set Up

Step 0: Start minikube

```
$ minikube start
```

Step 1: Install MinIO

```
$ kubectl apply -f kube/minio-deployment.yml
```

Step 2: Create bucket

- Port-forward the necessary ports

```
$ kubectl port-forward service/minio-service 8081:9090
```

- Navigate to console on local browser: http://localhost:8081/browser
    - user is `minioadmin`
    - pass is `minioadmin`
- In console, create bucket: `my-bucket`

Step 3: Deploy app.

```
$ export MINIO_ACCESS_KEY=minioadmin
$ export MINIO_SECRET_KEY=minioadmin
$ envsubst < kube/app-deployment.yml | kubectl apply -f -
```

## Description of CI/CD

Within Gitlab CI/CD:

- Runs tests
- Builds docker image
- Publishes docker image with tag `latest`

The `latest` tag is pulled at every CronJob run.

## Misc

I created a quick and dirty local API using Flask to mimic a public API.  It has 3 values: an UUID, an int, and a string.  I assume that the best partition key when the data grows is the int.

To minimize time, I saved the data as CSV; however, using a Hive-compatible format like orc or parquet would have made partitioning more viable.  Even better would have been a data lake format like Iceberg, Hudi, or Delta Lake.

## Cleaning Up
```
$ kubectl delete pod minio
$ kubectl delete svc minio-service
$ kubectl delete cronjob data-exporter
$ kubectl delete svc local-api-service
$ kubectl delete pod -l "service=data-exporter"
```
