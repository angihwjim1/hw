from unittest.mock import MagicMock, patch

import pytest

from data_export import DataExporter, LocalApiExporter, MinioApiExporter


def test_transform():
    class _k(DataExporter):
        def get(self):
            return 0
        def save(self, data):
            self._data = data
        def transform(self, data):
            return 1

    exporter = _k()
    exporter.run()
    assert exporter._data == 1


def test_handle_error_passed():
    class _k(DataExporter):
        def get(self):
            return 0
        def save(self, data):
            raise Exception
        def handle_error(self, error):
            pass

    _k().run()


def test_handle_error_raises():
    class _k(DataExporter):
        def get(self):
            return 0
        def save(self, data):
            raise Exception
        def handle_error(self, error):
            raise ValueError

    with pytest.raises(ValueError):
        _k().run()


@patch("requests.get")
def test_local_base_api_exporter_status_not_ok(patched_request):
    patched_request.return_value.ok = False

    exporter = LocalApiExporter(MagicMock())
    with pytest.raises(RuntimeError):
        exporter.get()


def test_minio_partition():
    input_data = {
        "my_key": [
            {"id": 25},
            {"id": 15_123},
            {"id": 299_020},
            {"id": 299_999},
        ]
    }

    exporter = MinioApiExporter(MagicMock())
    exporter.key = "my_key"
    exporter.partition_key = "id"

    actual = exporter.transform(input_data)

    expected = {
        0: [{"id": 25}],
        1: [{"id": 15_123}],
        29: [{"id": 299_020}, {"id": 299_999}]
    }
    assert expected == actual
