import json

from data_export import LocalApiExporter, MinioApiExporter


# register exporter classes here
JOB_CLASSES = {
    "LocalApiExporter": LocalApiExporter,
    "MinioApiExporter": MinioApiExporter,
}


def job_factory(job_attrs):
    is_active = job_attrs.get("is_active", True)
    if not is_active:
        return

    job_class = job_attrs["class"]
    job_config = job_attrs["config"]
    return JOB_CLASSES[job_class](job_config)


if __name__ == "__main__":
    with open("config.json", "r") as json_f:
        config_data = json.load(json_f)
    for job_name, job_attrs in config_data.items():

        job = job_factory(job_attrs)
        if job:
            print(f"Running job {job_name}")
            job.run()
        else:
            print(f"Skipping job {job_name}")
